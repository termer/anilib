﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AniLib.Animations
{
    public class Animation
    {
        
        private List<Element> mElements;

       

        public Animation()
        {
            mElements = new List<Element>();
        }

        public Element AddNewElement()
        {
            int id = 0;
            if (mElements.Count > 0)
            {
                id = mElements.Max(p => p.Id) + 1;
            }

            Element element = new Element(id);
            Add(element);

            return element;
        }

        public void Add(Element element)
        {
            mElements.Add(element);
        }

        public void Remove(Element element)
        {
            mElements.Remove(element);
        }

        public void InterpolateElementsTo(float time)
        {
            foreach (Element element in mElements)
            {
                ElementFrame frame = element.GetInterpolation(time);
                element.SetCurrentStatus(frame);
            }
        }

        public List<Element> Elements
        {
            get { return mElements; }
        }

        public float AnimationLength
        {
            get
            {
                float minTime = Elements.Min(p => p.MinTime);
                float maxTime = Elements.Max(p => p.AnimationLength);

                return maxTime - minTime;
            }
        }
    }
}
