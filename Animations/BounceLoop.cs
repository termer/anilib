﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AniLib.Animations
{
    public class BounceLoop:Loop
    {
        public override float LoopTime(float time)
        {
            while (time >= mLength)
            {
                time -= mLength;
            }
            return time;
        }

        protected override float GetDistance()
        {
            throw new NotImplementedException();
        }
    }
}
