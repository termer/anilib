﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AniLib.Math;

namespace AniLib.Animations
{
    [Serializable]
    public class Element : IPositionable
    {
        public int Id;

        public Vector2 Position { get; set; }
        public Vector2 Rotation { get; set; }

        public LoopType LoopType { get; set; }

        public List<ElementFrame> Frames;
        private ElementFrame mCurrentStatus;

        private LoopFactory mLoopFactory;

        public Element()
        {
            Frames = new List<ElementFrame>();
            LoopType = LoopType.RepeatLoop;
            mLoopFactory = new LoopFactory();
        }

        public Element(int id)
            : this()
        {
            Id = id;
        }

        public void SetFrame(ElementFrame frame)
        {
            UpdateFirstFrame(frame);

            int index = Frames.FindIndex(p => p.Time == frame.Time);

            if (index != -1)
            {
                Frames[index] = frame;
            }
            else
            {
                Frames.Add(frame);
                Frames.Sort();
            }
        }

        public void RemoveFrame(float time)
        {
            int index = Frames.FindIndex(p => p.Time == time);
            if (index != -1)
            {
                Frames.RemoveAt(index);
            }
        }

        public void ChangeElementTime(float oldTime, float newTime)
        {
            int index = Frames.FindIndex(p => p.Time == oldTime);

            ElementFrame frame = Frames[index];
            frame.Time = newTime;
            Frames[index] = frame;
        }


        public ElementFrame GetFrame(float time)
        {
            return Frames.Find(p => p.Time == time);
        }

        public void SetCurrentStatus(ElementFrame status)
        {
            mCurrentStatus = status;
        }

        public ElementFrame GetInterpolation(float time)
        {
            Loop loop = mLoopFactory.CreateLoop(LoopType);

            loop.SetLength(AnimationLength);

            float loopedTime = loop.LoopTime(time);

            ElementFrame leftFrame = mCurrentStatus;
            ElementFrame rightFrame = GetRightFrame(loopedTime);

            loop.SetTimes(leftFrame.Time, rightFrame.Time);

            float interpolationAmount = loop.GetInterpolatioAmount(loopedTime);

            ElementFrame interpolatedFrame = InterpolateFrame(leftFrame, rightFrame, interpolationAmount);

            interpolatedFrame.Time = loopedTime;

            return interpolatedFrame;
        }

        private static ElementFrame InterpolateFrame(ElementFrame leftFrame, ElementFrame rightFrame, float interpolationAmount)
        {
            ElementFrame interpolatedFrame = new ElementFrame
            {
                Position = Mathf.Lerp(leftFrame.Position, rightFrame.Position, interpolationAmount),
                Rotation = Mathf.LerpRotation(leftFrame.Rotation, rightFrame.Rotation, interpolationAmount)
            };

            return interpolatedFrame;
        }

        private void UpdateFirstFrame(ElementFrame frame)
        {
            if (!Frames.Any())
            {
                mCurrentStatus = frame;
            }
        }

        private ElementFrame GetRightFrame(float time)
        {
            ElementFrame frame = GetFrame(LastFrameIndex);

            for (int i = Frames.Count - 1; i >= 0; i--)
            {
                ElementFrame currentFrame = Frames[i];

                if (currentFrame.Time >= time)
                {
                    frame = currentFrame;
                }
                else
                {
                    break;
                }
            }

            return frame;
        }

        private ElementFrame GetLeftFrame(float time)
        {
            ElementFrame frame = GetFrame(FirstFrameIndex);

            for (int i = 0; i < Frames.Count; i++)
            {
                ElementFrame currentFrame = Frames[i];

                if (currentFrame.Time <= time)
                {
                    frame = currentFrame;
                }
                else
                {
                    break;
                }
            }

            return frame;
        }



        private ElementFrame GetFrame(int index)
        {
            if (Frames.Any()) 
            {
                return Frames[index];
            }
            return new ElementFrame();
        }

        public ElementFrame CurrentStatus
        {
            get { return mCurrentStatus; }
        }

        private int LastFrameIndex
        {
            get { return Frames.Count - 1; }
        }

        private static int FirstFrameIndex
        {
            get { return 0; }
        }

        public float MinTime
        {
            get
            {
                return Frames.Any() ? Frames[0].Time : 0f;
            }
        }

        public float AnimationLength
        {
            get { return Frames.Any() ? Frames[Frames.Count - 1].Time : 0f; }
        }
    }
}
