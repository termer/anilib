﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AniLib.Math;

namespace AniLib.Animations
{
    public struct ElementFrame : IComparable<ElementFrame>, IPositionable
    {
        public Vector2 Position { get; set; }
        public Vector2 Rotation { get; set; }

        public float Alpha;
        public float Time;
        public int CompareTo(ElementFrame other)
        {
            return System.Math.Sign(Time - other.Time);
        }
    }
}
