﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AniLib.Math;

namespace AniLib.Animations
{
    public interface IPositionable
    {
        Vector2 Position { get; set; }
        Vector2 Rotation { get; set; }
    }
}
