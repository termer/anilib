﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AniLib.Animations
{
    public enum LoopType
    {
        RepeatLoop,
        BounceLoop
    }

    public abstract class Loop
    {
        protected float mTimeLeft;
        protected float mTimeRight;
        protected float mLength;

        public abstract float LoopTime(float time);
        protected abstract float GetDistance();

        public void SetTimes(float timeLeft, float timeRight)
        {
            mTimeLeft = timeLeft;
            mTimeRight = timeRight;
        }

        public void SetLength(float length)
        {
            mLength = length;
        }

        protected virtual float TimePosition(float time)
        {
            if (mTimeLeft < mTimeRight)
            {
                if (time > mTimeLeft)
                {
                    return time - mTimeLeft;
                }
                return time;
            }

            float overallLength = mLength - mTimeLeft - time;
            return overallLength;
        }

        public virtual float GetInterpolatioAmount(float time)
        {
            float lengthBetweenFrames = GetDistance();
            float timePosition = TimePosition(time);
            float interpolationAmount = lengthBetweenFrames == 0 ? 1
                                                                 : timePosition / lengthBetweenFrames;
            return interpolationAmount;
        }
    }
}
