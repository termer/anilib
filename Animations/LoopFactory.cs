﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AniLib.Animations
{
    public class LoopFactory
    {
        public Loop CreateLoop(LoopType type)
        {
            switch (type)
            {
                case LoopType.RepeatLoop:
                    return new RepeatLoop();
                case LoopType.BounceLoop:
                    return new BounceLoop();
                default:
                    throw new ArgumentOutOfRangeException("type");
            }
        }
    }
}
