﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AniLib.Math;

namespace AniLib.Animations
{
    public class RepeatLoop:Loop
    {
        public override float LoopTime(float time)
        {
            if (mLength <= 0)
            {
                time = 0;
                return time;
            }

            while (time >= mLength)
            {
                time -= mLength;
            }

            if (time == mLength)
            {
                time = 0;
            }

            return time;
        }

        protected override float GetDistance()
        {
            if (mTimeRight < mTimeLeft)
            {
                return mLength - mTimeLeft + mTimeRight;
            }
            else
            {
                return Mathf.Abs(mTimeRight - mTimeLeft);
            }
        }
    }
}
