﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AniLib.Math
{
    static public class Mathf
    {
        public const float DegToRad = (float)(System.Math.PI/180.0);
        public const float RadToDeg = 1/DegToRad;


        public static float Lerp(float a, float b, float t)
        {
            return a + (b - a)*t;
        }

        public static Vector2 Lerp(Vector2 a, Vector2 b, float t)
        {
            return new Vector2(Lerp(a.X, b.X, t), Lerp(a.Y, b.Y, t));
        }


        public static Vector2 LerpRotation(Vector2 a, Vector2 b, float t)
        {
            float rotA = (float)System.Math.Atan2(a.Y, a.X);
            float rotB = (float)System.Math.Atan2(b.Y, b.X);

            float interpolatedRot = LerpDegrees(rotA, rotB, t);

            return new Vector2(Cos(interpolatedRot), Sin(interpolatedRot));
        }

        public static float LerpDegrees(float start, float end, float amount)
        {
            float difference = System.Math.Abs(end - start);

            if (difference > 180)
            {
                if (end > start)
                {
                    start += 360;
                }
                else
                {
                    end += 360;
                }
            }

            float value = (start + ((end - start) * amount));

            const float rangeZero = 360;

            if (value >= 0 && value <= 360)
                return value;

            return (value % rangeZero);
        }

        public static float Atan2(float y, float x)
        {
            return (float)System.Math.Atan2(y, x);
        }

        public static float Cos(float v)
        {
            return (float) System.Math.Cos(v);
        }

        public static float Sin(float v)
        {
            return (float)System.Math.Sin(v);
        }

        public static float Floor(float v)
        {
            return (float) System.Math.Floor(v);
        }

        public static float Abs(float v)
        {
            return (float) System.Math.Abs(v);
        }
    }
}
