﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AniLib.Math
{
    public struct Vector2
    {
        public float X;
        public float Y;

        public Vector2(float x, float y)
        {
            X = x;
            Y = y;
        }

        public static Vector2 AngleToVector(float angleRad)
        {
            return new Vector2(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
        }

        public static float VectorToAngle(Vector2 rotation)
        {
            return Mathf.Atan2(rotation.Y, rotation.X);
        }
    }
}
