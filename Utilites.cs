using System;
using System.Diagnostics;
using System.IO;


using System.Xml;
using System.Xml.Serialization;


using System.Collections.Generic;

namespace AniLib
{

    ///////////////////////////////////////////////////////////////////////////
    #region Utilites
    /// <summary>
    /// Class that contains utility methods.
    /// </summary>
    public static class Utilites
    {
        ///////////////////////////////////////////////////////////////////////////
        #region GetFilesInFolder

        public static List<FileInfo> GetFilesInFolder(string folderPath, string searchFor, bool recursive)
        {
            DirectoryInfo d = new DirectoryInfo(folderPath);
            List<FileInfo> fileList = new List<FileInfo>(d.GetFiles(searchFor));

            if (recursive)
            {
                DirectoryInfo[] dis = d.GetDirectories();

                foreach (DirectoryInfo di in dis)
                {
                    fileList.AddRange(GetFilesInFolder(di.FullName, searchFor, recursive));
                }
            }

            return fileList;
        }

        #endregion
        ///////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////
        #region Save

        /// <summary>
        /// Serializes specified object to a file
        /// </summary>
        /// <typeparam name="T">Type of data to write</typeparam>
        /// <param name="data">Data to save</param>
        /// <param name="fileName">File name of the file that contains Settings</param>
        public static void SerializeObject<T>(System.Object data, string fileName)
        {
            XmlSerializer srlz = null;
            XmlWriter writer = null;

            srlz = new XmlSerializer(typeof(T));

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.NewLineChars = Environment.NewLine;
            settings.NewLineOnAttributes = true;
            settings.NewLineHandling = NewLineHandling.Replace;
            settings.CloseOutput = true;
            settings.Indent = true;

            writer = XmlWriter.Create(fileName, settings);

            srlz.Serialize(writer, data);
            if (writer != null)
            {
                writer.Close();
            }
        }

        #endregion
        ///////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////
        #region Load

        /// <summary>
        /// Loads object from a file 
        /// </summary>
        /// <param name="fileName">File name of the file that contains Settings</param>
        /// <param name="type">Type of data to write</param>
        /// <returns>Loaded data</returns>
        public static T DeserializeObject<T>(string fileName) where T : class
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(fileName, FileMode.Open);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return null;
            }

            return DesiarilizeFromStream<T>(fs);
        }

        public static T DesiarilizeFromStream<T>(Stream fs) where T : class
        {
            StreamReader sr = new StreamReader(fs);
            StringReader strReader = new StringReader(sr.ReadToEnd());

            XmlSerializer srlz = null;
            XmlReader reader = null;
            T result = null;
            try
            {
                srlz = new XmlSerializer(typeof (T));
                reader = XmlReader.Create(strReader);
                result = srlz.Deserialize(reader) as T;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                result = null;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            if (strReader != null)
            {
                strReader.Close();
            }

            if (sr != null)
            {
                sr.Close();
            }
            if (fs != null)
            {
                fs.Close();
            }

            return result;
        }

        #endregion
        ///////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////
        #region GetValuesFromEnum
        ///// <summary>
        ///// Gets values from all fields of the specified type enum.
        ///// </summary>
        ///// <param name="t">Enum, whose values required.</param>
        ///// <returns>All values of all fields of the specified enum.</returns>
        //public static object[] GetValuesFromEnum(Type t)
        //{
        //    Array ar = Enum.GetValues(t);
        //    object[] o = new object[ar.Length];
        //    ar.CopyTo(o, 0);
        //    return o;
        //}

        #endregion
        ///////////////////////////////////////////////////////////////////////////

    

        ///////////////////////////////////////////////////////////////////////////
        #region EnablemeshRenderer

        #endregion
        /////////////////////////////////////////////////////////////////////////
        

		
    

        #endregion
        ///////////////////////////////////////////////////////////////////////////

    }
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    #region HslColor
    /// <summary>
    /// HSL color.
    /// </summary>
    public struct HslColor
    {
        public int h;	// [0,359]
        public float s;	// [0,1]
        public float l;	// [0,1]
    }
    #endregion
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    #region HsvColor
    /// <summary>
    /// HSL color.
    /// </summary>
    public struct HsvColor
    {
        public int h;	// [0,359]
        public float s;	// [0,1]
        public float v;	// [0,1]

        public override string ToString()
        {
            return "h = " + h + " s = " + s + " v = " + v;
        }
    }
    #endregion
    ///////////////////////////////////////////////////////////////////////////

}